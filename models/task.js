import { v4 as uuidv4 } from "uuid";

class Task {
  id = "";
  description = "";
  completedDate;

  constructor(description) {
    this.description = description;
    this.id = uuidv4();
    this.completedDate = null;
  }
}

export default Task;
