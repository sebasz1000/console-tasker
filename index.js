import colors from "colors";
//const { showMenu, pauseApp } = require("./helpers/messages");
import { menu, pauseApp } from "./helpers/inquirer.js";
import Task from "./models/task.js";
import Tasks from "./models/tasks.js";

const main = async () => {
  console.clear();
  let opt;
  do {
    const task = new Task("Comprar comida");
    const tasks = new Tasks();
    tasks._list[task.id] = task;
    //console.log({ tasks });
    0;
    opt = await menu();
    await pauseApp();
  } while (opt !== "0");
};

main();
