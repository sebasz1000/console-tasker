import inquirer from "inquirer";
import colors from "colors";

const questions = [
  {
    type: "list",
    name: "option", // important to deestructuration
    message: "¿What's next?",
    choices: [
      {
        value: "1",
        name: `${"1".yellow}. Create task`,
      },
      {
        value: "2",
        name: `${"2".yellow}. List task`,
      },
      {
        value: "3",
        name: `${"3".yellow}. List completed task`,
      },
      {
        value: "4",
        name: `${"4".yellow}. List pending task`,
      },
      {
        value: "5",
        name: `${"5".yellow}. Complete task`,
      },
      {
        value: "6",
        name: `${"6".yellow}. Remove task`,
      },
      {
        value: "0",
        name: `${"0".yellow}. Quit`,
      },
    ],
  },
];

const menu = async () => {
  console.clear();
  console.log("======================".yellow);
  console.log("  SELECT AN OPTION".yellow);
  console.log("====================== \n".yellow);

  // questions "name" property object
  const { option } = await inquirer.prompt(questions);

  return option;
};

const pauseApp = async () => {
  console.log("\n");
  console.log("*-------------------------*");

  await inquirer.prompt([
    {
      type: "input",
      name: "pushedEnter",
      message: `Press ${
        "ENTER".green
      } to continue  \n*-------------------------*`,
    },
  ]);

  console.log("\n");
};

export { menu, pauseApp };
