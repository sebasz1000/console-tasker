/* Example to use readline node's native language NOT USED IN INDEX.JS (Replaced by inquirer)*/
import colors from "colors";
//require('colors')  COMMON JS OLD VERSION

const showMenu = () => {
  return new Promise((resolve) => {
    console.clear();
    console.log("======================".yellow);
    console.log("  SELECT AN OPTION".yellow);
    console.log("====================== \n".yellow);

    console.log(`${"1.".yellow} Create task`);
    console.log(`${"2.".yellow} List tasks`);
    console.log(`${"3.".yellow} List completed task`);
    console.log(`${"4.".yellow} List pending task`);
    console.log(`${"5.".yellow} Complete task`);
    console.log(`${"6.".yellow} Delete task`);
    console.log(`${"0.".yellow} Exit \n`);

    //input-output values via console
    const readline = require("readline").createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    readline.question("Select and option: ", (opt) => {
      readline.close();
      resolve(opt);
    });
  }); //Promise
}; // showMenu

const pauseApp = () => {
  return new Promise((resolve) => {
    const readline = require("readline").createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    readline.question(`Press ${"ENTER".green} to continue`, (opt) => {
      readline.close();
      resolve();
    });
  });
};

module.exports = {
  showMenu,
  pauseApp,
};
